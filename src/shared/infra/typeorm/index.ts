import pg from "pg";
import { Connection, createConnection, getConnectionOptions } from "typeorm";

export default async (): Promise<Connection> => {
  pg.types.setTypeParser(
    1114,
    (stringValue: string) => new Date(`${stringValue}+0000`)
  );

  const defaultOptions = await getConnectionOptions();

  return createConnection(
    Object.assign(defaultOptions, {
      database:
        process.env.NODE_ENV === "test"
          ? process.env.POSTGRES_NAME_TEST
          : defaultOptions.database,
    })
  );
};
