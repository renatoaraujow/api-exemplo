import { MigrationInterface, QueryRunner, Table } from "typeorm";

export class CreateCustomers1630350251532 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: "customers",
        columns: [
          {
            name: "id",
            type: "uuid",
            isPrimary: true,
          },
          {
            name: "user_id",
            type: "uuid",
          },
          {
            name: "name",
            type: "varchar",
            length: "100",
          },
          {
            name: "phone",
            type: "varchar",
            length: "14",
          },
          {
            name: "email",
            type: "varchar",
            length: "250",
          },
          {
            name: "birth_date",
            type: "date",
          },
          {
            name: "gender",
            type: "varchar",
            length: "1",
          },
          {
            name: "height",
            type: "integer",
          },
          {
            name: "weight",
            type: "numeric",
            precision: 4,
            scale: 1,
          },
          {
            name: "created_at",
            type: "timestamp with time zone",
            default: "now()",
          },
          {
            name: "updated_at",
            type: "timestamp with time zone",
            default: "now()",
          },
          {
            name: "active",
            type: "boolean",
            default: true,
          },
        ],
        foreignKeys: [
          {
            name: "FKUserCustomer",
            referencedTableName: "users",
            referencedColumnNames: ["id"],
            columnNames: ["user_id"],
            onDelete: "SET NULL",
            onUpdate: "SET NULL",
          },
        ],
      })
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable("customers");
  }
}
