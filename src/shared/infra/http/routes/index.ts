import { Router } from "express";

import { authenticateRoutes } from "./authenticate.routes";
import { customersRoutes } from "./customers.routes";
import { usersRoutes } from "./users.routes";

const router = Router();

router.get("/", (request, response) => {
  response.send(
    `<h1 style="font-family: Arial;">Bem-vindo a API Exemplo!</h1>
    <p style="font-family: Arial; font-size: 16px;">Para acessar a documentação da API <a href="/api-docs">clique aqui</a>.</p>`
  );
});

router.use("/users", usersRoutes);
router.use("/auth", authenticateRoutes);
router.use("/customers", customersRoutes);

export { router };
