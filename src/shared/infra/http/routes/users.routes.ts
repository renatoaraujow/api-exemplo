import { celebrate, Segments, Joi } from "celebrate";
import { Router } from "express";

import { AccountUserController } from "@modules/users/useCases/accountUser/AccountUserController";
import { CreateUserController } from "@modules/users/useCases/createUser/CreateUserController";

import { ensureAuthenticated } from "../middlewares/ensureAuthenticated";

const usersRoutes = Router();

const createUserController = new CreateUserController();
const accountUserController = new AccountUserController();

usersRoutes.post(
  "/",
  celebrate({
    [Segments.BODY]: {
      name: Joi.string().required().max(100),
      email: Joi.string().email().required().max(250),
      password: Joi.string().required().min(6),
    },
  }),
  createUserController.handle
);

usersRoutes.get("/account", ensureAuthenticated, accountUserController.handle);

export { usersRoutes };
