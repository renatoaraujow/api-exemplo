import { ICreateUserTokenDTO } from "@modules/users/dtos/ICreateUserTokenDTO";
import { IFindByUserDTO } from "@modules/users/dtos/IFindByUserDTO";
import { UserToken } from "@modules/users/infra/typeorm/entities/UserToken";

import { IUsersTokensRepository } from "../IUsersTokensRepository";

class UsersTokensRepositoryInMemory implements IUsersTokensRepository {
  usersTokens: UserToken[] = [];

  async create({
    user_id,
    expires_date,
    refresh_token,
  }: ICreateUserTokenDTO): Promise<UserToken> {
    const userToken = new UserToken();

    Object.assign(userToken, {
      user_id,
      expires_date,
      refresh_token,
    });

    this.usersTokens.push(userToken);

    return userToken;
  }

  async findByUserIdAndRefreshToken({
    user_id,
    refresh_token,
  }: IFindByUserDTO): Promise<UserToken> {
    const token = this.usersTokens.find(
      (tokenInMemory) =>
        tokenInMemory.user_id === user_id &&
        tokenInMemory.refresh_token === refresh_token
    );

    return token;
  }

  async deleteById(id: string): Promise<void> {
    const token = this.usersTokens.find(
      (tokenInMemory) => tokenInMemory.id === id
    );

    this.usersTokens.splice(this.usersTokens.indexOf(token));
  }
}

export { UsersTokensRepositoryInMemory };
