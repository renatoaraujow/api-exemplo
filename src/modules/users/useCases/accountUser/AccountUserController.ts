import { Request, Response } from "express";
import { container } from "tsyringe";

import { AccountUserUseCase } from "./AccountUserUseCase";

class AccountUserController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { id: user_id } = request.user;

    const accountUserUseCase = container.resolve(AccountUserUseCase);

    const user = await accountUserUseCase.execute(user_id);

    return response.json(user);
  }
}

export { AccountUserController };
