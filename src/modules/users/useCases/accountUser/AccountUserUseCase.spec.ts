import { v4 as uuidV4 } from "uuid";

import { User } from "@modules/users/infra/typeorm/entities/User";
import { UsersRepositoryInMemory } from "@modules/users/repositories/in-memory/UsersRepositoryInMemory";
import { CreateUserUseCase } from "@modules/users/useCases/createUser/CreateUserUseCase";
import { AppError } from "@shared/errors/AppError";

import { AccountUserUseCase } from "./AccountUserUseCase";

let usersRepositoryInMemory: UsersRepositoryInMemory;

let createUserUseCase: CreateUserUseCase;
let accountUserUseCase: AccountUserUseCase;

let userId: string;

describe("Account User", () => {
  beforeEach(async () => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
    accountUserUseCase = new AccountUserUseCase(usersRepositoryInMemory);

    const user = await createUserUseCase.execute({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    userId = user.id;
  });

  it("should be able to read the user's account", async () => {
    const userAccount = await accountUserUseCase.execute(userId);

    expect(userAccount).toBeInstanceOf(User);
    expect(userAccount).toHaveProperty("id");
  });

  it("should not be able to read a non-existent user's account", async () => {
    await expect(accountUserUseCase.execute(uuidV4())).rejects.toEqual(
      new AppError("User not found")
    );
  });
});
