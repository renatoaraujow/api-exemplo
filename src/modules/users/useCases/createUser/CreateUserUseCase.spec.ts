import { User } from "@modules/users/infra/typeorm/entities/User";
import { UsersRepositoryInMemory } from "@modules/users/repositories/in-memory/UsersRepositoryInMemory";
import { AppError } from "@shared/errors/AppError";

import { CreateUserUseCase } from "./CreateUserUseCase";

let createUserUseCase: CreateUserUseCase;
let usersRepositoryInMemory: UsersRepositoryInMemory;

describe("Create User", () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();
    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
  });

  it("should be able to create a new user", async () => {
    const user = await createUserUseCase.execute({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    expect(user).toBeInstanceOf(User);
    expect(user).toHaveProperty("id");
  });

  it("should not be able to create a new user if there is another one with the same email", async () => {
    await createUserUseCase.execute({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    await expect(
      createUserUseCase.execute({
        name: "User Test",
        email: "user@example.com",
        password: "123456",
      })
    ).rejects.toEqual(new AppError("User already exists"));
  });
});
