import request from "supertest";
import { Connection } from "typeorm";

import { app } from "@shared/infra/http/app";
import createConnection from "@shared/infra/typeorm";

let connection: Connection;
describe("Create User Controller", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("should be able to create a new user ", async () => {
    const response = await request(app).post("/users").send({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    expect(response.status).toBe(201);
  });

  it("should not be able to create a new user with the same email", async () => {
    const response = await request(app).post("/users").send({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    expect(response.status).toBe(400);
  });
});
