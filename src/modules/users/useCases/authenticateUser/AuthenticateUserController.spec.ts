import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import { app } from "@shared/infra/http/app";
import createConnection from "@shared/infra/typeorm";

let connection: Connection;
describe("Authenticate User Controller", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO users(id, name, email, password, created_at, updated_at, active)
        values('${id}', 'User Test', 'user@example.com', '${password}', 'now()', 'now()', true)`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("should be able to authenticate a user", async () => {
    const response = await request(app).post("/auth/sessions").send({
      email: "user@example.com",
      password: "123456",
    });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("user");
    expect(response.body).toHaveProperty("access_token");
    expect(response.body).toHaveProperty("refresh_token");
    expect(response.body.user.email).toEqual("user@example.com");
  });

  it("should not be able to authenticate a user with incorrect email", async () => {
    const response = await request(app).post("/auth/sessions").send({
      email: "false@example.com",
      password: "123456",
    });

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty("message");
    expect(response.body.message).toEqual("Email or password incorrect!");
  });

  it("should not be able to authenticate a user with incorrect password", async () => {
    const response = await request(app).post("/auth/sessions").send({
      email: "user@example.com",
      password: "1234567",
    });

    expect(response.status).toBe(400);
    expect(response.body).toHaveProperty("message");
    expect(response.body.message).toEqual("Email or password incorrect!");
  });
});
