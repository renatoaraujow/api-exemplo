import { JsonWebTokenError, sign } from "jsonwebtoken";

import auth from "@config/auth";
import { ICreateUserDTO } from "@modules/users/dtos/ICreateUserDTO";
import { UsersRepositoryInMemory } from "@modules/users/repositories/in-memory/UsersRepositoryInMemory";
import { UsersTokensRepositoryInMemory } from "@modules/users/repositories/in-memory/UsersTokensRepositoryInMemory";
import { DayjsDateProvider } from "@shared/container/providers/DateProvider/implementations/DayjsDateProvider";
import { AppError } from "@shared/errors/AppError";

import { AuthenticateUserUseCase } from "../authenticateUser/AuthenticateUserUseCase";
import { CreateUserUseCase } from "../createUser/CreateUserUseCase";
import { RefreshTokenUseCase } from "./RefreshTokenUseCase";

let usersRepositoryInMemory: UsersRepositoryInMemory;
let usersTokensRepositoryInMemory: UsersTokensRepositoryInMemory;
let dateProvider: DayjsDateProvider;

let createUserUseCase: CreateUserUseCase;
let authenticateUserUseCase: AuthenticateUserUseCase;
let refreshTokenUseCase: RefreshTokenUseCase;

describe("Refresh Token", () => {
  beforeEach(() => {
    usersRepositoryInMemory = new UsersRepositoryInMemory();
    usersTokensRepositoryInMemory = new UsersTokensRepositoryInMemory();
    dateProvider = new DayjsDateProvider();

    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
    authenticateUserUseCase = new AuthenticateUserUseCase(
      usersRepositoryInMemory,
      usersTokensRepositoryInMemory,
      dateProvider
    );
    refreshTokenUseCase = new RefreshTokenUseCase(
      usersTokensRepositoryInMemory,
      dateProvider
    );
  });

  it("should be able to generate a new token", async () => {
    const user: ICreateUserDTO = {
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    };

    await createUserUseCase.execute(user);

    const authentication = await authenticateUserUseCase.execute({
      email: user.email,
      password: user.password,
    });

    const refreshToken = await refreshTokenUseCase.execute(
      authentication.refresh_token
    );

    expect(refreshToken).toHaveProperty("access_token");
    expect(refreshToken).toHaveProperty("refresh_token");
  });

  it("should not be able to generate a refresh token for a non-existent token", async () => {
    const invalidRefreshToken = sign(
      { email: "false@example.com" },
      auth.secret_refresh_token
    );

    await expect(
      refreshTokenUseCase.execute(invalidRefreshToken)
    ).rejects.toEqual(new AppError("Refresh Token does not exists!"));
  });

  it("should not be able to generate a refresh token for a invalid signature", async () => {
    const invalidRefreshToken = sign(
      { email: "false@example.com" },
      "invalidSignature"
    );

    await expect(
      refreshTokenUseCase.execute(invalidRefreshToken)
    ).rejects.toBeInstanceOf(JsonWebTokenError);
  });

  it("should not be able to generate a refresh token for a invalid jwt", async () => {
    const invalidRefreshToken = "invalidJwt";

    await expect(
      refreshTokenUseCase.execute(invalidRefreshToken)
    ).rejects.toBeInstanceOf(JsonWebTokenError);
  });
});
