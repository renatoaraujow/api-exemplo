import { Request, Response } from "express";
import { container } from "tsyringe";

import { RefreshTokenUseCase } from "./RefreshTokenUseCase";

class RefreshTokenController {
  async handle(request: Request, response: Response): Promise<Response> {
    const refreshToken =
      request.body.refresh_token ||
      request.headers["x-access-token"] ||
      request.query.refresh_token;

    const refreshTokenUseCase = container.resolve(RefreshTokenUseCase);

    const accessTokenAndRefreshToken = await refreshTokenUseCase.execute(
      refreshToken
    );

    return response.json(accessTokenAndRefreshToken);
  }
}

export { RefreshTokenController };
