import { ICreateCustomerDTO } from "@modules/customers/dtos/ICreateCustomerDTO";
import { IFindByUserDTO } from "@modules/customers/dtos/IFindByUserDTO";
import { IFindByEmailDTO } from "@modules/customers/dtos/IFindByEmailDTO";
import { IFindByIdDTO } from "@modules/customers/dtos/IFindByIdDTO";
import { IFindByPhoneDTO } from "@modules/customers/dtos/IFindByPhoneDTO";
import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";

import { ICustomersRepository } from "../ICustomersRepository";

class CustomersRepositoryInMemory implements ICustomersRepository {
  customers: Customer[] = [];

  async create({
    user_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: ICreateCustomerDTO): Promise<Customer> {
    const customer = new Customer();

    Object.assign(customer, {
      user_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    this.customers.push(customer);

    return customer;
  }

  async findByUser({
    user_id,
    offset,
    limit,
  }: IFindByUserDTO): Promise<Customer[]> {
    const customers = this.customers.filter(
      (customerInMemory) => customerInMemory.user_id === user_id
    );

    if (offset && limit) {
      return customers.slice(offset, limit + 1);
    }

    return customers;
  }

  async findByIdAndUserId({
    user_id,
    customer_id,
  }: IFindByIdDTO): Promise<Customer> {
    const customer = this.customers.find(
      (customerInMemory) =>
        customerInMemory.user_id === user_id &&
        customerInMemory.id === customer_id
    );

    return customer;
  }

  async findByEmailAndUserId({
    user_id,
    email,
  }: IFindByEmailDTO): Promise<Customer> {
    const customer = this.customers.find(
      (customerInMemory) =>
        customerInMemory.user_id === user_id &&
        customerInMemory.email === email
    );

    return customer;
  }

  async findByPhoneAndUserId({
    user_id,
    phone,
  }: IFindByPhoneDTO): Promise<Customer> {
    const customer = this.customers.find(
      (customerInMemory) =>
        customerInMemory.user_id === user_id &&
        customerInMemory.phone === phone
    );

    return customer;
  }

  async save(customer: Customer): Promise<void> {
    const findIndex = this.customers.findIndex(
      (customerInMemory) => customerInMemory.id === customer.id
    );

    this.customers[findIndex] = customer;
  }

  async delete(customer_id: string): Promise<void> {
    const customer = this.customers.find(
      (customerInMemory) => customerInMemory.id === customer_id
    );

    this.customers.splice(this.customers.indexOf(customer));
  }
}

export { CustomersRepositoryInMemory };
