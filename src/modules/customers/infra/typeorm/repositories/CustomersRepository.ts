import { getRepository, Repository } from "typeorm";

import { ICreateCustomerDTO } from "@modules/customers/dtos/ICreateCustomerDTO";
import { IFindByUserDTO } from "@modules/customers/dtos/IFindByUserDTO";
import { IFindByEmailDTO } from "@modules/customers/dtos/IFindByEmailDTO";
import { IFindByIdDTO } from "@modules/customers/dtos/IFindByIdDTO";
import { IFindByPhoneDTO } from "@modules/customers/dtos/IFindByPhoneDTO";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";

import { Customer } from "../entities/Customer";

class CustomersRepository implements ICustomersRepository {
  private repository: Repository<Customer>;

  constructor() {
    this.repository = getRepository(Customer);
  }

  async create({
    user_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: ICreateCustomerDTO): Promise<Customer> {
    const customer = this.repository.create({
      user_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    await this.repository.save(customer);

    return customer;
  }

  async findByEmailAndUserId({
    email,
    user_id,
  }: IFindByEmailDTO): Promise<Customer> {
    const customer = await this.repository.findOne({
      email,
      user_id,
      active: true,
    });
    return customer;
  }

  async findByPhoneAndUserId({
    phone,
    user_id,
  }: IFindByPhoneDTO): Promise<Customer> {
    const customer = await this.repository.findOne({
      phone,
      user_id,
      active: true,
    });
    return customer;
  }

  async findByUser({
    user_id,
    offset,
    limit,
  }: IFindByUserDTO): Promise<Customer[]> {
    const findOptions = {
      where: { user_id, active: true },
    };

    if (offset) {
      Object.assign(findOptions, { skip: offset });
    }

    if (limit) {
      Object.assign(findOptions, { take: limit });
    }

    const customers = await this.repository.find(findOptions);

    return customers;
  }

  async findByIdAndUserId({
    customer_id,
    user_id,
    active,
  }: IFindByIdDTO): Promise<Customer> {
    const where = {
      id: customer_id,
      user_id,
    };

    if (active === true) {
      Object.assign(where, { active: true });
    }

    const customer = await this.repository.findOne(where);

    return customer;
  }

  async save(customer: Customer): Promise<void> {
    this.repository.save(customer);
  }

  async delete(customer_id: string): Promise<void> {
    this.repository.update({ id: customer_id }, { active: false });
  }
}

export { CustomersRepository };
