interface IListCustomersDTO {
  user_id: string;
  offset?: number;
  limit?: number;
}

export { IListCustomersDTO };
