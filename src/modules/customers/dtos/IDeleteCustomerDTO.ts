interface IDeleteCustomerDTO {
  customer_id: string;
  user_id: string;
}

export { IDeleteCustomerDTO };
