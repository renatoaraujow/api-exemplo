interface IReadCustomerDTO {
  customer_id: string;
  user_id: string;
}

export { IReadCustomerDTO };
