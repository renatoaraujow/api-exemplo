import { Request, Response } from "express";
import { container } from "tsyringe";

import { ListCustomersUseCase } from "./ListCustomersUseCase";

class ListCustomersController {
  async handle(request: Request, response: Response): Promise<Response> {
    const offset = request.query.offset as string;
    const limit = request.query.limit as string;

    const { id: user_id } = request.user;

    const listCustomersUseCase = container.resolve(ListCustomersUseCase);

    const customers = await listCustomersUseCase.execute({
      user_id,
      offset: Number(offset),
      limit: Number(limit),
    });

    return response.json(customers);
  }
}

export { ListCustomersController };
