import { v4 as uuidV4 } from "uuid";

import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { CustomersRepositoryInMemory } from "@modules/customers/repositories/in-memory/CustomersRepositoryInMemory";
import { UsersRepositoryInMemory } from "@modules/users/repositories/in-memory/UsersRepositoryInMemory";
import { CreateUserUseCase } from "@modules/users/useCases/createUser/CreateUserUseCase";
import { AppError } from "@shared/errors/AppError";

import { CreateCustomerUseCase } from "../createCustomer/CreateCustomerUseCase";
import { ListCustomersUseCase } from "./ListCustomersUseCase";

let customersRepositoryInMemory: CustomersRepositoryInMemory;
let usersRepositoryInMemory: UsersRepositoryInMemory;

let createUserUseCase: CreateUserUseCase;
let createCustomerUseCase: CreateCustomerUseCase;
let listCustomersUseCase: ListCustomersUseCase;

let userId: string;
let customer: Customer;

describe("List Customers", () => {
  beforeEach(async () => {
    customersRepositoryInMemory = new CustomersRepositoryInMemory();
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
    createCustomerUseCase = new CreateCustomerUseCase(
      customersRepositoryInMemory,
      usersRepositoryInMemory
    );
    listCustomersUseCase = new ListCustomersUseCase(
      customersRepositoryInMemory,
      usersRepositoryInMemory
    );

    const user = await createUserUseCase.execute({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    userId = user.id;

    customer = await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548999669966",
      email: "customer@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });
  });

  it("should be able to list customers", async () => {
    const customers = await listCustomersUseCase.execute({
      user_id: customer.user_id,
    });

    expect(customers).toEqual([customer]);
  });

  it("should not be able to list customers to a non-existent user", async () => {
    await expect(
      listCustomersUseCase.execute({
        user_id: uuidV4(),
      })
    ).rejects.toEqual(new AppError("User not found"));
  });

  it("should be able to list customers with offset and limit", async () => {
    const customerTwo = await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test Two",
      phone: "+5548966666666",
      email: "customer2@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test Three",
      phone: "+5548999999999",
      email: "customer3@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const customers = await listCustomersUseCase.execute({
      user_id: userId,
      offset: 1,
      limit: 1,
    });

    expect(customers).toEqual([customerTwo]);
    expect(customers.length).toBe(1);
  });
});
