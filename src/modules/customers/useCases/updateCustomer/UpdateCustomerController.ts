import { Request, Response } from "express";
import { container } from "tsyringe";

import { UpdateCustomerUseCase } from "./UpdateCustomerUseCase";

class UpdateCustomerController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { name, phone, email, birth_date, gender, height, weight } =
      request.body;
    const { customer_id } = request.params;
    const { id: user_id } = request.user;

    const updateCustomerUseCase = container.resolve(UpdateCustomerUseCase);

    await updateCustomerUseCase.execute({
      customer_id,
      user_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    return response.status(204).send();
  }
}

export { UpdateCustomerController };
