import { inject, injectable } from "tsyringe";

import { IUpdateCustomerDTO } from "@modules/customers/dtos/IUpdateCustomerDTO";
import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { IUsersRepository } from "@modules/users/repositories/IUsersRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class UpdateCustomerUseCase {
  constructor(
    @inject("CustomersRepository")
    private customersRepository: ICustomersRepository,
    @inject("UsersRepository")
    private usersRepository: IUsersRepository
  ) {}

  async execute({
    customer_id,
    user_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: IUpdateCustomerDTO): Promise<Customer> {
    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError("User not found");
    }

    const customer = await this.customersRepository.findByIdAndUserId({
      customer_id,
      user_id,
      active: true,
    });

    if (!customer) {
      throw new AppError("Customer not found");
    }

    const emailAlreadyExists =
      await this.customersRepository.findByEmailAndUserId({
        email,
        user_id,
      });

    if (emailAlreadyExists && emailAlreadyExists.id !== customer_id) {
      throw new AppError("Email already exists");
    }

    const phoneAlreadyExists =
      await this.customersRepository.findByPhoneAndUserId({
        phone,
        user_id,
      });

    if (phoneAlreadyExists && phoneAlreadyExists.id !== customer_id) {
      throw new AppError("Phone already exists");
    }

    const newBirthDate = new Date(birth_date).toISOString();

    Object.assign(customer, {
      name,
      phone,
      email,
      birth_date: newBirthDate,
      gender,
      height,
      weight,
    });

    this.customersRepository.save(customer);

    return customer;
  }
}

export { UpdateCustomerUseCase };
