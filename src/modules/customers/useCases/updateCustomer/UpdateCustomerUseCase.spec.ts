import { v4 as uuidV4 } from "uuid";

import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { CustomersRepositoryInMemory } from "@modules/customers/repositories/in-memory/CustomersRepositoryInMemory";
import { UsersRepositoryInMemory } from "@modules/users/repositories/in-memory/UsersRepositoryInMemory";
import { CreateUserUseCase } from "@modules/users/useCases/createUser/CreateUserUseCase";
import { AppError } from "@shared/errors/AppError";

import { CreateCustomerUseCase } from "../createCustomer/CreateCustomerUseCase";
import { UpdateCustomerUseCase } from "./UpdateCustomerUseCase";

let customersRepositoryInMemory: CustomersRepositoryInMemory;
let usersRepositoryInMemory: UsersRepositoryInMemory;

let createUserUseCase: CreateUserUseCase;
let createCustomerUseCase: CreateCustomerUseCase;
let updateCustomerUseCase: UpdateCustomerUseCase;

let userId: string;
let customer: Customer;

describe("Update Customer", () => {
  beforeEach(async () => {
    customersRepositoryInMemory = new CustomersRepositoryInMemory();
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
    createCustomerUseCase = new CreateCustomerUseCase(
      customersRepositoryInMemory,
      usersRepositoryInMemory
    );
    updateCustomerUseCase = new UpdateCustomerUseCase(
      customersRepositoryInMemory,
      usersRepositoryInMemory
    );

    const user = await createUserUseCase.execute({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    userId = user.id;

    customer = await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548999669966",
      email: "customer@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });
  });

  it("should be able to update a customer", async () => {
    const customerUpdated = await updateCustomerUseCase.execute({
      customer_id: customer.id,
      user_id: customer.user_id,
      name: "Customer Test Updated",
      phone: "+5548999669966",
      email: "customerupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const customerFound = await customersRepositoryInMemory.findByIdAndUserId({
      customer_id: customer.id,
      user_id: userId,
    });

    expect(customerFound.name).toBe(customerUpdated.name);
    expect(customerFound.email).toBe(customerUpdated.email);
  });

  it("should not be able to update a customer if there is another one with the same email", async () => {
    await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548966666666",
      email: "customerupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await expect(
      updateCustomerUseCase.execute({
        user_id: userId,
        customer_id: customer.id,
        name: "Customer Test",
        phone: "+5548999669966",
        email: "customerupdated@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Email already exists"));
  });

  it("should not be able to update a customer if there is another one with the same phone", async () => {
    await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548966666666",
      email: "customerupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await expect(
      updateCustomerUseCase.execute({
        user_id: userId,
        customer_id: customer.id,
        name: "Customer Test",
        phone: "+5548966666666",
        email: "customer@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Phone already exists"));
  });

  it("should be able to update a customer with the same email/phone if it is a different user", async () => {
    const userTwo = await createUserUseCase.execute({
      name: "User Test Two",
      email: "user2@example.com",
      password: "123456",
    });

    await createCustomerUseCase.execute({
      user_id: userTwo.id,
      name: "Customer Test",
      phone: "+5548966666666",
      email: "customerupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const customerUpdated = await updateCustomerUseCase.execute({
      user_id: userId,
      customer_id: customer.id,
      name: "Customer Test",
      phone: "+5548966666666",
      email: "customerupdated@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const customerFound = await customersRepositoryInMemory.findByIdAndUserId({
      customer_id: customer.id,
      user_id: userId,
    });

    expect(customerFound.email).toBe(customerUpdated.email);
  });

  it("should not be able to update a customer to a non-existent user", async () => {
    await expect(
      updateCustomerUseCase.execute({
        user_id: uuidV4(),
        customer_id: customer.id,
        name: "Customer Test",
        phone: "+5548999669966",
        email: "customerupdated@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("User not found"));
  });

  it("should not be able to update a non-existent customer", async () => {
    await expect(
      updateCustomerUseCase.execute({
        user_id: userId,
        customer_id: uuidV4(),
        name: "Customer Test",
        phone: "+5548999669966",
        email: "customerupdated@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Customer not found"));
  });
});
