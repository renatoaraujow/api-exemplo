import { inject, injectable } from "tsyringe";

import { ICreateCustomerDTO } from "@modules/customers/dtos/ICreateCustomerDTO";
import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { IUsersRepository } from "@modules/users/repositories/IUsersRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class CreateCustomerUseCase {
  constructor(
    @inject("CustomersRepository")
    private customersRepository: ICustomersRepository,
    @inject("UsersRepository")
    private usersRepository: IUsersRepository
  ) {}

  async execute({
    user_id,
    name,
    phone,
    email,
    birth_date,
    gender,
    height,
    weight,
  }: ICreateCustomerDTO): Promise<Customer> {
    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError("User not found");
    }

    const emailAlreadyExists =
      await this.customersRepository.findByEmailAndUserId({
        email,
        user_id,
      });

    if (emailAlreadyExists) {
      throw new AppError("Email already exists");
    }

    const phoneAlreadyExists =
      await this.customersRepository.findByPhoneAndUserId({
        phone,
        user_id,
      });

    if (phoneAlreadyExists) {
      throw new AppError("Phone already exists");
    }

    const newBirthDate = new Date(birth_date).toISOString();

    const customer = await this.customersRepository.create({
      user_id,
      name,
      phone,
      email,
      birth_date: newBirthDate,
      gender,
      height,
      weight,
    });

    return customer;
  }
}

export { CreateCustomerUseCase };
