import { v4 as uuidV4 } from "uuid";

import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { CustomersRepositoryInMemory } from "@modules/customers/repositories/in-memory/CustomersRepositoryInMemory";
import { UsersRepositoryInMemory } from "@modules/users/repositories/in-memory/UsersRepositoryInMemory";
import { CreateUserUseCase } from "@modules/users/useCases/createUser/CreateUserUseCase";
import { AppError } from "@shared/errors/AppError";

import { CreateCustomerUseCase } from "./CreateCustomerUseCase";

let customersRepositoryInMemory: CustomersRepositoryInMemory;
let usersRepositoryInMemory: UsersRepositoryInMemory;

let createUserUseCase: CreateUserUseCase;
let createCustomerUseCase: CreateCustomerUseCase;

let userId: string;

describe("Create Customer", () => {
  beforeEach(async () => {
    customersRepositoryInMemory = new CustomersRepositoryInMemory();
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
    createCustomerUseCase = new CreateCustomerUseCase(
      customersRepositoryInMemory,
      usersRepositoryInMemory
    );

    const user = await createUserUseCase.execute({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    userId = user.id;
  });

  it("should be able to create a new customer", async () => {
    const customer = await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548999669966",
      email: "customer@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    expect(customer).toBeInstanceOf(Customer);
    expect(customer).toHaveProperty("id");
  });

  it("should not be able to create a new customer if there is another one with the same email", async () => {
    await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548999669966",
      email: "customer@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await expect(
      createCustomerUseCase.execute({
        user_id: userId,
        name: "Customer Test",
        phone: "+5548999669966",
        email: "customer@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Email already exists"));
  });

  it("should not be able to create a new customer if there is another one with the same phone", async () => {
    await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548999669966",
      email: "customer@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    await expect(
      createCustomerUseCase.execute({
        user_id: userId,
        name: "Customer Test",
        phone: "+5548999669966",
        email: "customer2@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("Phone already exists"));
  });

  it("should be able to create a new customer with the same email/phone if it is a different user", async () => {
    const userTwo = await createUserUseCase.execute({
      name: "User Test Two",
      email: "user2@example.com",
      password: "123456",
    });

    await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548999669966",
      email: "customer@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    const customer = await createCustomerUseCase.execute({
      user_id: userTwo.id,
      name: "Customer Test",
      phone: "+5548999669966",
      email: "customer@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });

    expect(customer).toBeInstanceOf(Customer);
    expect(customer).toHaveProperty("id");
  });

  it("should not be able to add a new customer to a non-existent user", async () => {
    await expect(
      createCustomerUseCase.execute({
        user_id: uuidV4(),
        name: "Customer Test",
        phone: "+5548999669966",
        email: "customer@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
    ).rejects.toEqual(new AppError("User not found"));
  });
});
