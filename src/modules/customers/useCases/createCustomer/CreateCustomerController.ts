import { Request, Response } from "express";
import { container } from "tsyringe";

import { CreateCustomerUseCase } from "./CreateCustomerUseCase";

class CreateCustomerController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { name, phone, email, birth_date, gender, height, weight } =
      request.body;
    const { id: user_id } = request.user;

    const createCustomerUseCase = container.resolve(CreateCustomerUseCase);

    await createCustomerUseCase.execute({
      user_id,
      name,
      phone,
      email,
      birth_date,
      gender,
      height,
      weight,
    });

    return response.status(201).send();
  }
}

export { CreateCustomerController };
