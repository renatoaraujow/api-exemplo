import { hash } from "bcrypt";
import request from "supertest";
import { Connection } from "typeorm";
import { v4 as uuid } from "uuid";

import { app } from "@shared/infra/http/app";
import createConnection from "@shared/infra/typeorm";

let connection: Connection;
describe("Read Customer Controller", () => {
  beforeAll(async () => {
    connection = await createConnection();
    await connection.runMigrations();

    const id = uuid();
    const password = await hash("123456", 8);

    await connection.query(
      `INSERT INTO users(id, name, email, password, created_at, updated_at, active)
        values('${id}', 'User Test', 'user@example.com', '${password}', 'now()', 'now()', true)`
    );
  });

  afterAll(async () => {
    await connection.dropDatabase();
    await connection.close();
  });

  it("should be able to read a customer", async () => {
    const responseAuth = await request(app).post("/auth/sessions").send({
      email: "user@example.com",
      password: "123456",
    });

    const { access_token } = responseAuth.body;

    await request(app)
      .post("/customers")
      .send({
        name: "Customer Test",
        phone: "+5548999669966",
        email: "customer@example.com",
        birth_date: "1990-12-28",
        gender: "M",
        height: 180,
        weight: 98.7,
      })
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const responseList = await request(app)
      .get("/customers")
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    const customers = responseList.body;

    const response = await request(app)
      .get(`/customers/${customers[0].id}`)
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(200);
    expect(response.body).toHaveProperty("id");
    expect(response.body.email).toEqual("customer@example.com");
  });

  it("should not be able to read a non-existant customer", async () => {
    const responseAuth = await request(app).post("/auth/sessions").send({
      email: "user@example.com",
      password: "123456",
    });

    const { access_token } = responseAuth.body;

    const response = await request(app)
      .get(`/customers/${uuid()}`)
      .set({
        Authorization: `Bearer ${access_token}`,
      });

    expect(response.status).toBe(400);
  });
});
