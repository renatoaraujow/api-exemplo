import { inject, injectable } from "tsyringe";

import { IReadCustomerDTO } from "@modules/customers/dtos/IReadCustomerDTO";
import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { ICustomersRepository } from "@modules/customers/repositories/ICustomersRepository";
import { IUsersRepository } from "@modules/users/repositories/IUsersRepository";
import { AppError } from "@shared/errors/AppError";

@injectable()
class ReadCustomerUseCase {
  constructor(
    @inject("CustomersRepository")
    private customersRepository: ICustomersRepository,
    @inject("UsersRepository")
    private usersRepository: IUsersRepository
  ) {}

  async execute({
    customer_id,
    user_id,
  }: IReadCustomerDTO): Promise<Customer> {
    const user = await this.usersRepository.findById(user_id);

    if (!user) {
      throw new AppError("User not found");
    }

    const customer = await this.customersRepository.findByIdAndUserId({
      customer_id,
      user_id,
      active: true,
    });

    if (!customer) {
      throw new AppError("Customer not found");
    }

    return customer;
  }
}

export { ReadCustomerUseCase };
