import { Request, Response } from "express";
import { container } from "tsyringe";

import { DeleteCustomerUseCase } from "./DeleteCustomerUseCase";

class DeleteCustomerController {
  async handle(request: Request, response: Response): Promise<Response> {
    const { customer_id } = request.params;
    const { id: user_id } = request.user;

    const deleteCustomerUseCase = container.resolve(DeleteCustomerUseCase);

    await deleteCustomerUseCase.execute({
      customer_id,
      user_id,
    });

    return response.status(204).send();
  }
}

export { DeleteCustomerController };
