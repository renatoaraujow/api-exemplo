import { v4 as uuidV4 } from "uuid";

import { Customer } from "@modules/customers/infra/typeorm/entities/Customer";
import { CustomersRepositoryInMemory } from "@modules/customers/repositories/in-memory/CustomersRepositoryInMemory";
import { UsersRepositoryInMemory } from "@modules/users/repositories/in-memory/UsersRepositoryInMemory";
import { CreateUserUseCase } from "@modules/users/useCases/createUser/CreateUserUseCase";
import { AppError } from "@shared/errors/AppError";

import { CreateCustomerUseCase } from "../createCustomer/CreateCustomerUseCase";
import { DeleteCustomerUseCase } from "./DeleteCustomerUseCase";

let usersRepositoryInMemory: UsersRepositoryInMemory;
let customersRepositoryInMemory: CustomersRepositoryInMemory;

let createUserUseCase: CreateUserUseCase;
let createCustomerUseCase: CreateCustomerUseCase;
let deleteCustomerUseCase: DeleteCustomerUseCase;

let userId: string;
let customer: Customer;

describe("Delete Customer", () => {
  beforeEach(async () => {
    customersRepositoryInMemory = new CustomersRepositoryInMemory();
    usersRepositoryInMemory = new UsersRepositoryInMemory();

    createUserUseCase = new CreateUserUseCase(usersRepositoryInMemory);
    createCustomerUseCase = new CreateCustomerUseCase(
      customersRepositoryInMemory,
      usersRepositoryInMemory
    );
    deleteCustomerUseCase = new DeleteCustomerUseCase(
      customersRepositoryInMemory,
      usersRepositoryInMemory
    );

    const user = await createUserUseCase.execute({
      name: "User Test",
      email: "user@example.com",
      password: "123456",
    });

    userId = user.id;

    customer = await createCustomerUseCase.execute({
      user_id: userId,
      name: "Customer Test",
      phone: "+5548999669966",
      email: "customer@example.com",
      birth_date: "1990-12-28",
      gender: "M",
      height: 180,
      weight: 98.7,
    });
  });

  it("should be able to delete customers", async () => {
    expect(
      await deleteCustomerUseCase.execute({
        user_id: userId,
        customer_id: customer.id,
      })
    ).not.toBeInstanceOf(AppError);
  });

  it("should not be able to delete customers to a non-existent user", async () => {
    await expect(
      deleteCustomerUseCase.execute({
        user_id: uuidV4(),
        customer_id: customer.id,
      })
    ).rejects.toEqual(new AppError("User not found"));
  });

  it("should not be able to delete non-existent customers", async () => {
    await expect(
      deleteCustomerUseCase.execute({
        user_id: userId,
        customer_id: uuidV4(),
      })
    ).rejects.toEqual(new AppError("Customer not found"));
  });
});
